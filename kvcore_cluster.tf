terraform {
    required_providers {
        azurestack = {
            source = "hashicorp/azurestack"
            version = "0.9.0"
        }
    }
}

variable "subid" {
    type = string
}

variable "clientid" {
    type = string
}

variable "clientsecret" {
    type = string
}

variable "tenant" {
    type = string
}

variable "cluster_name" {
    type = string
}

variable "az_location" {
    type = string
}

provider "azurerm" {
    version = "=2.4.0"
    features {}
    subscription_id = var.subid
    client_id = var.clientid
    client_secret = var.clientsecret
    tenant_id = var.tenant
}

resource "azurerm_kubernetes_cluster" "cluster01" {
    name = var.cluster_name
    location = var.az_location
    resource_group_name = "LAB01"
    dns_prefix = var.cluster_name
    service_principal {
        client_id = var.clientid
        client_secret = var.clientsecret

    }

    default_node_pool {
        name = var.cluster_name
        node_count = 3
        vm_size = "Standard_D2_v2"
    }

    identity {
        type = "SystemAssigned"
    }
}

resource "azurerm_managed_disk" "wp-config-disk" {
    name = var.cluster_name
    location = var.az_location
    storage_account_type = "Premium_LRS"
    disk_size_gb = "16"
    create_option = "Empty"
    resource_group_name = azurerm_kubernetes_cluster.cluster01.node_resource_group
    tags = {
        environment = var.cluster_name
    }
}

output "client_certificate" {
    value = azurerm_kubernetes_cluster.cluster01.kube_config.0.client_certificate
    sensitive = true
}

output "kube_config" {
    value = azurerm_kubernetes_cluster.cluster01.kube_config_raw
    sensitive = true
}