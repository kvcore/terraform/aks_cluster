package main

import (
	"fmt"
	"html/template"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

var Tpl *template.Template
var formName string
var formEmail string
var clusterName string
var location string

func init() {
	Tpl = template.Must(template.ParseGlob("*.gohtml"))
}

func main() {

	http.HandleFunc("/", createDev)
	http.ListenAndServe(":80", nil)

}

func createDev(w http.ResponseWriter, req *http.Request) {

	Tpl.ExecuteTemplate(w, "createdev.gohtml", nil)

	if req.Method == http.MethodPost {
		s := make([]int, 5)
		z := []string{}

		for range s {
			rand.Seed(time.Now().UnixNano())
			id := rand.Intn(len(s))
			s = append(s[1:5], id)
		}

		for i := range s {
			number := s[i]
			text := strconv.Itoa(number)
			z = append(z, text)
		}

		if req.Method == http.MethodPost {
			location = req.FormValue("region")
		}

		go build("cluster"+strings.Join(z, ""), location)
	}
}

func build(cluster string, location string) {
	workingDir, err := os.Getwd()
	checkError(err)

	err = os.Chdir(workingDir)
	checkError(err)

	err = os.Mkdir("/tmp/"+cluster, 0777)
	checkError(err)

	os.Setenv("dir", "/tmp/"+cluster)
	os.Setenv("state_file", "/tmp/"+cluster+"/"+cluster+"_PLAN")
	os.Setenv("cluster_name", cluster)
	os.Setenv("az_location", location)

	command := exec.Command("/bin/bash", workingDir+"/wrapper.sh")
	log.Printf("Running wrapper....")

	out, err := command.CombinedOutput()
	checkError(err)
	fmt.Println(string(out))
}

func checkError(e error) {
	if e != nil {
		log.Println(e)
	}
}
