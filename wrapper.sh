#!/bin/bash
echo $dir
echo $cluster_name
echo $state_file
echo $az_location


export dir=$dir
export state=$state_file
export TF_VAR_cluster_name=qapipeline
export TF_VAR_subid=<sub_id>
export TF_VAR_clientid=<clent_id>
export TF_VAR_clientsecret=<client_id>
export TF_VAR_tenant=<tenant_id>
export TF_VAR_az_location="East US"

terraform init

terraform plan -out $state_file

#Copy the main terraform file to the new temp dir
cp kvcore_cluster.tf $dir

cd $dir

terraform init

terraform apply -auto-approve

terraform output -json > /tmp/$cluster_name/$cluster_name.json
